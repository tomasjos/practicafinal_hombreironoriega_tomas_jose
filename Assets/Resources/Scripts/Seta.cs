﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seta : MonoBehaviour
{
    string result = "";
    public float minspeed=2;
    public float maxspeed = 4;
    private float velocidadenemigo;
    private float actualspeed;
    GameObject mienemigo;
    bool limitederecho;
    bool limiteizquierdo;
    public GameObject Explosion;
    private float x, y, z;
    // Start is called before the first frame update
    void Start()
    {
        x = gameObject.transform.position.x;
        y = gameObject.transform.position.y;
        limiteizquierdo = false;
        limitederecho = false;
        
    }
    void Awake()
    {
        mienemigo = gameObject;
        alternarenemigo();
    }
    void alternarenemigo()
    {
        actualspeed = Random.Range(minspeed, maxspeed);
      /*  x = Random.Range(-3f, 3f);
        y = 0f;
        z = 0f;*/

    }
    // Update is called once per frame
    void Update()
    {
        velocidadenemigo = actualspeed * Time.deltaTime;
        if( (x - mienemigo.transform.position.x <= 1.20) &&(limiteizquierdo==false))
        {
            limiteizquierdo = false;
            limitederecho = false;
         //   Debug.Log("Estoy a menos de dos unidades de distancia por la izquierda");
            mienemigo.transform.Translate(Vector3.left * velocidadenemigo);
        }
        else if((x-mienemigo.transform.position.x>1.20) &&(limiteizquierdo==false))
        {
            limiteizquierdo = true;
            //Debug.Log("me desplazo hacia la derecha ");
           
        }
        else if((x-mienemigo.transform.position.x>=-1.20)&&(limiteizquierdo==true))
        {
            mienemigo.transform.Translate(Vector3.right * velocidadenemigo);
        }
        else if ((x - mienemigo.transform.position.x <-1.20) && (limiteizquierdo == true))
        {
            limiteizquierdo = false;
            limitederecho = true;
        }



      /*  if (mienemigo.transform.position.x - x <= 2)
        {
            Debug.Log("Estoy a menos de dos unidades de distancia por la derecha");
            mienemigo.transform.Translate(Vector3.right * velocidadenemigo);
        }
        else if(mienemigo.transform.position.x-x>2)
        {
            Debug.Log("me desplazo hacia la izquierda");
            while (mienemigo.transform.position.x - x >=-2)
            {
                mienemigo.transform.Translate(Vector3.left * velocidadenemigo);
            }
        }*/


            // mienemigo.transform.Translate(Vector3.left * velocidadenemigo);
    }
    public GameplayManager GameplayManager;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (((collision.gameObject.CompareTag("Persona"))&&(collision.gameObject.GetComponent<MovimientoPersona>().super==false)))
        {
            result= "Game Over Perdiste";
            Debug.Log("Perdiste, chocaste con una seta");
            //  Invoke("reiniciarnivel", 2f);
           // Invoke("empezardesdeposicion", 2f);

           
        }
        if ((collision.gameObject.CompareTag("misil")))
        {
            //Invoke("destruir", 2f);
            Debug.Log("misil choca con seta");
            Explosion.transform.position = gameObject.transform.position;
            gameObject.SetActive(false);
            Explosion.GetComponent<ParticleSystem>().Play();
            Destroy(gameObject);
        }
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Persona"))
        {
            GameplayManager.RestartLevel();
        }
    }
    private void reiniciarnivel()
        {
        GameplayManager.RestartLevel();
    }
    
}
