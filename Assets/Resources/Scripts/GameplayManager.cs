﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{
   // public BikeController BikeController;
    public MovimientoPersona MovimientoPersona;
    public Text MoneyText;
    public Text PointsText;
    public Text TimeText;

    public Button StartButton;
    public Text Result;
    public Text TimeRecord;
    public Text PointsRecord;
    public Text MoneyRecord;
    private float initialTime;
    private float recordTime;
    private int recordPoints;
    private int recordMoney;
    private int secondsToStart = 3;

    private void Start()
    {
        TimeText.text = MoneyText.text = PointsText.text =Result.text=string.Empty;
        TimeRecord.text = PointsRecord.text = MoneyRecord.text = string.Empty;
     /*   BikeController.OnKilled += RestartLevel;
        BikeController.OnReachedEndOfLevel += EndGame;
        BikeController.enabled = false;*/
        MovimientoPersona.OnKilled += RestartLevel;
        MovimientoPersona.OnReachedEndOfLevel += EndGame;
        MovimientoPersona.enabled = false;
        recordTime = PlayerPrefs.GetFloat("time record level " + SceneManager.GetActiveScene().buildIndex, 0);
        recordPoints = PlayerPrefs.GetInt("points record level " + SceneManager.GetActiveScene().buildIndex, 0);
        recordMoney = PlayerPrefs.GetInt("money record level " + SceneManager.GetActiveScene().buildIndex, 0);
        Debug.Log("recordtime");
        Debug.Log(recordTime);

       // recordPoints=PlayerPrefs.GetFloat("")
        if (recordTime>0)
           TimeRecord.text = "Record: " + recordTime.ToString("00.00") + " s";
        if (recordPoints>0)
           PointsRecord.text = "Record: " + recordPoints.ToString() + " pts";
        if(recordMoney>0)
           MoneyRecord.text = "Record: " + recordMoney.ToString() + " uds";



    }

    private void Update()
    {
        // MovimientoPersona.distanciatierra = 0.5f;
        // Debug.Log(MovimientoPersona.enabled);
        if (MovimientoPersona.enabled)
        {
            TimeText.text = "Time: " + (Time.time - initialTime).ToString("00.00") + " s";
            PointsText.text = "Puntos: " + (MovimientoPersona.puntos).ToString() + " pts";
            MoneyText.text = "Monedas: " + (MovimientoPersona.monedas).ToString() + " uds";
            TimeRecord.text = "Record: " + recordTime.ToString("00:00") + " s";
            PointsRecord.text = "Record: " + recordPoints.ToString() + " pts";
            MoneyRecord.text = "Record: " + recordMoney.ToString() + " uds";
            if ((MovimientoPersona.perder == false) &&(MovimientoPersona.ganar==false))
            {
               Result.text = "En juego";
            }
            else if ((MovimientoPersona.perder==true) &&(MovimientoPersona.ganar==false))
            {
                Result.text = "Game Over";
            }
            else if((MovimientoPersona.perder==false)&&(MovimientoPersona.ganar==true))
            {
                Result.text = "Superaste el nivel!";
            }

        }
    /*    if(BikeController.enabled)
        {
            TimeText.text = "Time: " + (Time.time - initialTime).ToString("00.00") + "s";
        }*/
    }

    public void StartGame()
    {
        StartButton.gameObject.SetActive(false);
        Debug.Log("boton");
        TimeText.text = secondsToStart.ToString();
        InvokeRepeating(nameof(Countdown), 1, 1);
    }

    private void Countdown()
    {
        secondsToStart--;
        Debug.Log(secondsToStart);
        if (secondsToStart <= 0)
        {
            Debug.Log("llegue a cero");
            CancelInvoke();
            OnGameStarted();
        }
        else
            TimeText.text = secondsToStart.ToString();
    }

    public void RestartLevel()
    {
        MovimientoPersona.yamurio = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void OnGameStarted()
    {
        MovimientoPersona.enabled = true;
       // BikeController.enabled = true;
        initialTime = Time.time;
        TimeText.text = string.Empty;
    }

    public void EndGame()
    {
        StartButton.gameObject.SetActive(true);
       // BikeController.enabled = false;
        MovimientoPersona.enabled = false;
        TimeText.text = "FINAL! " + (Time.time - initialTime).ToString("00.00") + " s";
        if (recordTime==0)
        {
            PlayerPrefs.SetFloat("time record level " + SceneManager.GetActiveScene().buildIndex, (Time.time - initialTime));
            recordTime = PlayerPrefs.GetFloat("time record level " + SceneManager.GetActiveScene().buildIndex, (Time.time - initialTime));
            TimeText.text = "NEW RECORD! " + (Time.time - initialTime).ToString("00.00") + "s";
            TimeRecord.text = "Record: " + recordTime.ToString("00:00") + " s";
        }
        else
        {
            if ((Time.time - initialTime) < recordTime)
            {
                PlayerPrefs.SetFloat("time record level " + SceneManager.GetActiveScene().buildIndex, (Time.time - initialTime));
                TimeText.text = "NEW RECORD! " + (Time.time - initialTime).ToString("00.00") + "s";
                TimeRecord.text = "Record: " + recordTime.ToString("00:00") + " s";
            }
            else
            {
                TimeText.text = "FINAL! " + (Time.time - initialTime).ToString("00.00") + "s";
            }
        }
        if (recordPoints == 0)
        {
            PlayerPrefs.SetInt("points record level " + SceneManager.GetActiveScene().buildIndex, MovimientoPersona.puntos);
            recordPoints = PlayerPrefs.GetInt("points record level " + SceneManager.GetActiveScene().buildIndex);
            PointsText.text = "NEW RECORD! " + (recordPoints).ToString() + "pts";
            PointsRecord.text = "Record: " + recordPoints.ToString() + " pts";
        }
        else
        {
            if ((MovimientoPersona.puntos) < recordPoints)
            {
                PlayerPrefs.SetInt("points record level " + SceneManager.GetActiveScene().buildIndex, MovimientoPersona.puntos);
                PointsText.text = "NEW RECORD! " + (recordPoints).ToString() + "pts";
                PointsRecord.text = "Record: " + recordPoints.ToString() + " pts";
            }
            else
            {
                PointsText.text = "FINAL! " + MovimientoPersona.puntos.ToString() + " pts";
            }
        }
        if (recordMoney == 0)
        {
            PlayerPrefs.SetInt("money record level " + SceneManager.GetActiveScene().buildIndex, MovimientoPersona.monedas);
            recordMoney = PlayerPrefs.GetInt("money record level " + SceneManager.GetActiveScene().buildIndex);
            MoneyText.text = "NEW RECORD! " + (recordMoney).ToString() + "pts";
            MoneyRecord.text = "Record: " + recordMoney.ToString() + " pts";
        }
        else
        {
            if ((MovimientoPersona.monedas) < recordPoints)
            {
                PlayerPrefs.SetInt("money record level " + SceneManager.GetActiveScene().buildIndex, MovimientoPersona.monedas);
                MoneyText.text = "NEW RECORD! " + (recordMoney).ToString() + "pts";
                MoneyRecord.text = "Record: " + recordMoney.ToString() + " pts";
            }
            else
            {
                MoneyText.text = "FINAL! " + MovimientoPersona.monedas.ToString() + " pts";
            }
        }

    }
}
