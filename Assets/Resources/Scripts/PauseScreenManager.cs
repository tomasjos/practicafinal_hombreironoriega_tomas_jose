﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScreenManager : MonoBehaviour
{
    public GameObject PauseScreen;
    public void Start()
    {
        PauseScreen.SetActive(false);
    }
    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
        Time.timeScale = 1;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Level1");
        Time.timeScale = 1;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseScreen.SetActive(!PauseScreen.activeSelf);
            if (Time.timeScale == 1)
                Time.timeScale = 0;
            else if(Time.timeScale==0)
                Time.timeScale = 1;
        }
            
        
    }
}
