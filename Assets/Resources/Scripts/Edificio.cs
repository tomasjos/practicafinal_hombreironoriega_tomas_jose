﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Edificio : MonoBehaviour
{
    // Start is called before the first frame update
    string result = "";
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public GameplayManager GameplayManager;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Scene escena=SceneManager.GetActiveScene();
        if ((collision.gameObject.CompareTag("Persona"))&& escena.name=="Level1")
        {
            result = "Ganaste el nivel!!";
            Debug.Log("Ganaste el nivel");
            GameplayManager.Result.text = "Ganaste el nivel!!";
            GameplayManager.EndGame();
            Invoke("pasarnivel", 2f);

        }
        else if ((collision.gameObject.CompareTag("Persona")) && escena.name == "Level3")
        {
            result = "Ganaste la partida!!";
            Debug.Log("Ganaste la partida");
            GameplayManager.Result.text = "Ganaste la partida!!";
            GameplayManager.EndGame();
            Invoke("reiniciarnivel", 2f);

        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.CompareTag("Persona")))
        {

            GameplayManager.RestartLevel();
        }
    }
    private void reiniciarnivel()
    {
        SceneManager.LoadScene("MainMenu");
      //  GameplayManager.RestartLevel();
    }
    private void pasarnivel()
    {
        SceneManager.LoadScene("Level3");
    }
}
